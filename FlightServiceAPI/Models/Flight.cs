﻿using System;

namespace FlightServiceAPI.Models
{
    public class Flight
    {
        public int FlightId { get; set; }
        public string FlightCode { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string AirlineName { get; set; }
        public int Duration { get; set; }
        public int Price { get; set; }
        public int EmptySeets { get; set; }
        public DateTime FlightTime { get; set; }
    }
}
