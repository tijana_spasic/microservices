﻿using FlightServiceAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace FlightServiceAPI.DBContexts
{
    public class FlightContext : DbContext
    {
        public FlightContext(DbContextOptions<FlightContext> options) : base(options)
        {
        }
        public DbSet<Flight> Flights { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Flight>().HasKey(entity => entity.FlightId);
            modelBuilder.Entity<Flight>().ToTable("Flight");

            base.OnModelCreating(modelBuilder);
        }
    }
}
