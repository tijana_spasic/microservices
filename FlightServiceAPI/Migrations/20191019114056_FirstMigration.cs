﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FlightServiceAPI.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FlightCode",
                table: "Flight",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Flight",
                keyColumn: "FlightId",
                keyValue: 1,
                columns: new[] { "FlightCode", "FlightTime" },
                values: new object[] { "1111", new DateTime(2019, 10, 19, 11, 40, 55, 476, DateTimeKind.Utc).AddTicks(4901) });

            migrationBuilder.UpdateData(
                table: "Flight",
                keyColumn: "FlightId",
                keyValue: 2,
                columns: new[] { "FlightCode", "FlightTime" },
                values: new object[] { "2222", new DateTime(2019, 10, 19, 11, 40, 55, 476, DateTimeKind.Utc).AddTicks(6487) });

            migrationBuilder.UpdateData(
                table: "Flight",
                keyColumn: "FlightId",
                keyValue: 3,
                columns: new[] { "FlightCode", "FlightTime" },
                values: new object[] { "3333", new DateTime(2019, 10, 19, 11, 40, 55, 476, DateTimeKind.Utc).AddTicks(6506) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FlightCode",
                table: "Flight");

            migrationBuilder.UpdateData(
                table: "Flight",
                keyColumn: "FlightId",
                keyValue: 1,
                column: "FlightTime",
                value: new DateTime(2019, 10, 19, 10, 30, 49, 244, DateTimeKind.Utc).AddTicks(3674));

            migrationBuilder.UpdateData(
                table: "Flight",
                keyColumn: "FlightId",
                keyValue: 2,
                column: "FlightTime",
                value: new DateTime(2019, 10, 19, 10, 30, 49, 244, DateTimeKind.Utc).AddTicks(5829));

            migrationBuilder.UpdateData(
                table: "Flight",
                keyColumn: "FlightId",
                keyValue: 3,
                column: "FlightTime",
                value: new DateTime(2019, 10, 19, 10, 30, 49, 244, DateTimeKind.Utc).AddTicks(5876));
        }
    }
}
