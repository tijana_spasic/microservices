﻿using FlightServiceAPI.Commands;
using FlightServiceAPI.Events;
using FlightServiceAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightServiceAPI.Mappers
{
    public static class Mapper
    {
        public static FlightRegistered MapToFlightRegistered(this
            RegisterFlight command) => new FlightRegistered
            (
                Guid.NewGuid(),
                command.FlightId,
                command.FlightCode,
                command.From,
                command.To,
                command.AirlineName,
                command.Duration,
                command.Price,
                command.EmptySeets,
                command.FlightTime
            );
        public static Flight MapToFlight(this
            RegisterFlight command) => new Flight
            {
                FlightCode=command.FlightCode,
                AirlineName=command.AirlineName,
                Duration=command.Duration,
                EmptySeets=command.EmptySeets,
                FlightTime=DateTime.UtcNow,
                From=command.From,
                To=command.To,
                Price=command.Price
            };
    }
}
