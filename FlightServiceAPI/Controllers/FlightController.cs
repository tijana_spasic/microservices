﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using FlightServiceAPI.Commands;
using FlightServiceAPI.Events;
using FlightServiceAPI.Mappers;
using FlightServiceAPI.Models;
using FlightServiceAPI.Repository;
using Infrastructure.Messaging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FlightServiceAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightController : ControllerBase
    {
        private readonly IFlightRepository _flightRepository;
        private readonly IMessagePublisher _messagePublisher;

        public FlightController(IFlightRepository flightRepository, IMessagePublisher messagePublisher)
        {
            _flightRepository = flightRepository;
            _messagePublisher = messagePublisher;
        }
        // GET: api/Flight
        [HttpGet]
        public async Task<IEnumerable<Flight>> Get()
        {
            return await _flightRepository.GetFlights();
        }

        [HttpGet]
        [Route("{flightId}", Name = "GetByFlightId")]
        public async Task<IActionResult> GetByFlightId(int flightId)
        {
            var flight = await _flightRepository.GetFlightById(flightId);
            if (flight == null)
            {
                return NotFound();
            }
            return Ok(flight);
        }

        // POST: api/Flight
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RegisterFlight command)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // insert customer
                    Flight flight = command.MapToFlight();
                    await _flightRepository.InsertFlight(flight);

                    FlightRegistered e = command.MapToFlightRegistered();
                    await _messagePublisher.PublishMessageAsync(e.MessageType, e, "");

                    // return result
                    return CreatedAtRoute("GetByFlightId", new { flightId = flight.FlightId }, flight);
                }
                return BadRequest();
            }
            catch (DbUpdateException)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
                return StatusCode(StatusCodes.Status500InternalServerError);
                throw;
            }
        }

        [HttpGet]
        [Route("SearchFlight")]
        public async Task<IEnumerable<Flight>> SearchFlight(string from, string to, DateTime date)
        {
            return await _flightRepository.SearchFlight(from, to, date);
        }


        // PUT: api/Flight/5
        [HttpPut("{id}")]
        public async void Put(int id, [FromBody] Flight flight)
        {
            if (flight != null)
            {
                using (var scope = new TransactionScope())
                {
                    await _flightRepository.UpdateFlight(flight);
                    scope.Complete();
                }
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await _flightRepository.DeleteFlight(id);
        }
    }
}
