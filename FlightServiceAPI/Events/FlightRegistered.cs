﻿using Infrastructure.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightServiceAPI.Events
{
    public class FlightRegistered : Event
    {
        public readonly int FlightId;
        public readonly string FlightCode;
        public readonly string From;
        public readonly string To;
        public readonly string AirlineName;
        public readonly int Duration;
        public readonly int Price;
        public readonly int EmptySeets;
        public readonly DateTime FlightTime;

        public FlightRegistered(Guid messageId, int flightId, string flightCode, string from,
            string to, string airlineName, int duration, int price, int emptySeets, DateTime date) : base(messageId, messageType: "FlightRegistered")
        {
            FlightId = flightId;
            FlightCode = flightCode;
            From = from;
            To = to;
            AirlineName = airlineName;
            Duration = duration;
            Price = price;
            EmptySeets = emptySeets;
            FlightTime = date;
        }
    }
}
