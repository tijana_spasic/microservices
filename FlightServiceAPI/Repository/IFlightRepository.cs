﻿using FlightServiceAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightServiceAPI.Repository
{
    public interface IFlightRepository
    {
        Task<IEnumerable<Flight>> GetFlights();
        Task<IEnumerable<Flight>> SearchFlight(string from, string to, DateTime date);
        Task<Flight> GetFlightById(int FlightId);
        Task InsertFlight(Flight Flight);
        Task DeleteFlight(int FlightId);
        Task UpdateFlight(Flight Flight);
        Task SaveAsync();
    }
}
