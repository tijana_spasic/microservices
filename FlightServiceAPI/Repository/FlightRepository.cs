﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlightServiceAPI.DBContexts;
using FlightServiceAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace FlightServiceAPI.Repository
{
    public class FlightRepository : IFlightRepository
    {
        private readonly FlightContext _dbContext;
        public FlightRepository(FlightContext flightContext)
        {
            _dbContext = flightContext;
        }
        public Task DeleteFlight(int FlightId)
        {
            var flight = _dbContext.Flights.Find(FlightId);
            _dbContext.Flights.Remove(flight);
            return SaveAsync();
        }

        public Task<Flight> GetFlightById(int FlightId)
        {
            return _dbContext.Flights.FindAsync(FlightId).AsTask();
        }

        public async Task<IEnumerable<Flight>> GetFlights()
        {
            return await _dbContext.Flights.ToListAsync();
        }

        public async Task<IEnumerable<Flight>> SearchFlight(string from, string to, DateTime date)
        {
            return await _dbContext.Flights.Where(flight =>
                                        (from == null || flight.From == from) &&
                                        (to == null || flight.To == to)).ToListAsync();
        }

        public async Task InsertFlight(Flight Flight)
        {
            await _dbContext.Flights.AddAsync(Flight);
            await SaveAsync();
        }

        public Task SaveAsync()
        {
            return _dbContext.SaveChangesAsync();

        }

        public Task UpdateFlight(Flight Flight)
        {
            _dbContext.Entry(Flight).State = EntityState.Modified;
            return SaveAsync();
        }
    }
}
