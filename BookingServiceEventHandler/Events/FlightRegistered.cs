﻿using Infrastructure.Messaging;
using System;

namespace BookingServiceEventHandler.Events
{
    public class FlightRegistered : Event
    {
        public readonly int FlightId;
        public readonly string FlightCode;
        public readonly string AirlineName;
        public readonly int Price;
        public readonly int EmptySeets;

        public FlightRegistered(Guid messageId, int flightId, string flightCode, 
            string airlineName,  int price, int emptySeets) : base(messageId)
        {
            FlightId = flightId;
            FlightCode = flightCode;
            AirlineName = airlineName;
            Price = price;
            EmptySeets = emptySeets;
        }
    }
}
