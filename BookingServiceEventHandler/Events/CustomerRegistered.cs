﻿using Infrastructure.Messaging;
using System;

namespace BookingServiceEventHandler.Events
{
    public class CustomerRegistered : Event
    {
        public readonly int CustomerId;
        public readonly string Name;

        public CustomerRegistered(Guid messageId, int customerId, string name) :base(messageId)
        {
            CustomerId = customerId;
            Name = name;
        }
    }
}
