﻿using BookingServiceEventHandler.Models;
using Infrastructure.Messaging;
using System;

namespace BookingServiceEventHandler.Events
{
    public class FlightBooked : Event
    {
        public readonly int BookedFlightId;
        public readonly Flight Flight;
        public readonly Customer Customer;
        public readonly string Description;
        public readonly int NumberOfSeets;
        public FlightBooked(Guid messageId, int id, Flight flight, Customer customer, 
            string description, int numberOdSeets) : base(messageId)
        {
            BookedFlightId = id;
            Flight = flight;
            Customer = customer;
            Description = description;
            NumberOfSeets = numberOdSeets;
        }
    }
}
