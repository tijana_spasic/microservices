﻿using BookingServiceEventHandler.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookingServiceEventHandler.Repository
{
    public interface IBookingRepository
    {
        Task SaveAsync();
        Task InsertCustomer(Customer Customer);
        Task InsertReservation(Reservation reservation);
        Task InsertFlight(Flight Flight);

    }
}
