﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BookingServiceEventHandler.DBContexts;
using BookingServiceEventHandler.Models;

namespace BookingServiceEventHandler.Repository
{
    public class BookingRepository : IBookingRepository
    {
        private readonly BookingContext _dbContext;

        public BookingRepository(BookingContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task InsertReservation(Reservation reservation)
        {
            await _dbContext.Reservations.AddAsync(reservation);
            await SaveAsync();
        }

        public async Task InsertCustomer(Customer Customer)
        {
            await _dbContext.Customers.AddAsync(Customer);
            await SaveAsync();
        }

        public async Task InsertFlight(Flight Flight)
        {
            await _dbContext.Flights.AddAsync(Flight);
            await SaveAsync();
        }

        public Task SaveAsync()
        {
            return _dbContext.SaveChangesAsync();
        }
    }
}
