﻿using Microsoft.EntityFrameworkCore;
using Polly;
using System;

namespace BookingServiceEventHandler.DBContexts
{
    public static class DBInitializer
    {
        public static void Initialize(BookingContext context)
        {
            Policy
              .Handle<Exception>()
              .WaitAndRetry(10, r => TimeSpan.FromSeconds(10))
              .Execute(() => context.Database.Migrate());
        }
    }
}
