﻿using BookingServiceEventHandler.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookingServiceEventHandler.DBContexts
{
    public class BookingContext : DbContext
    {
        public BookingContext(DbContextOptions<BookingContext> options) :base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Flight> Flights { get; set; } 
        public DbSet<Reservation> Reservations { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Flight>().HasKey(entity => entity.FlightId);
            builder.Entity<Flight>().ToTable("Flight");

            builder.Entity<Customer>().HasKey(entity => entity.CustomerId);
            builder.Entity<Customer>().ToTable("Customer");

            builder.Entity<Reservation>().HasKey(entity => entity.ReservationId);
            builder.Entity<Reservation>().ToTable("Reservation");

            builder.Entity<Flight>().HasData(
               new Flight
               {
                   FlightId = 1,
                   FlightCode = "1111",
                   EmptySeets = 20,
                   Price = 120,
                   AirlineName = "AirSerbia"
               },
               new Flight
               {
                   FlightId = 2,
                   FlightCode = "2222",
                   EmptySeets = 10,
                   Price = 120,
                   AirlineName = "AirSerbia"
               },
               new Flight
               {
                   FlightId = 3,
                   FlightCode = "3333",
                   EmptySeets = 20,
                   Price = 120,
                   AirlineName = "AirSerbia"
               });

            base.OnModelCreating(builder);
        }

    }
}
