﻿using BookingServiceEventHandler.DBContexts;
using BookingServiceEventHandler.Repository;
using Infrastructure.Messaging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Polly;
using RabbitMQ.Client;
using Serilog;
using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;

namespace BookingServiceEventHandler
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            await host.RunAsync();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args)
                .ConfigureHostConfiguration(configHost =>
                {
                    configHost.SetBasePath(Directory.GetCurrentDirectory());
                    configHost.AddJsonFile("hostsettings.json", optional: true);
                    configHost.AddEnvironmentVariables();
                    configHost.AddEnvironmentVariables("BOOKING_");
                    configHost.AddCommandLine(args);
                })
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    config.AddJsonFile($"appsettings.json");
                })
                .ConfigureServices((hostContext, services) =>
                {
                    
                    services.AddTransient<IMessageHandler>((svc) =>
                    {
                        var rabbitMQConfigSection = hostContext.Configuration.GetSection("RabbitMQ");
                        string rabbitMQHost = rabbitMQConfigSection["Host"];
                        string rabbitMQUserName = rabbitMQConfigSection["Username"];
                        string rabbitMQPassword = rabbitMQConfigSection["Password"];

                        return new RabbitMQMessageHandler(rabbitMQHost, rabbitMQUserName, rabbitMQPassword, "exchange_booking", "BookingService", ""); ;
                    });

                    services.AddTransient<IBookingRepository, BookingRepository>();
                    services.AddDbContext<BookingContext>(o => o.UseSqlServer(hostContext.Configuration.GetConnectionString("booking_db")));


                    //services.AddTransient<BookingContext>((svc) =>
                    //{
                    //    var sqlConnectionString = hostContext.Configuration.GetConnectionString("booking_db");
                    //    var dbContextOptions = new DbContextOptionsBuilder<BookingContext>()
                    //        .UseSqlServer(sqlConnectionString)
                    //        .Options;
                    //    var dbContext = new BookingContext(dbContextOptions);

                    //    Policy
                    //        .Handle<Exception>()
                    //        .WaitAndRetry(10, r => TimeSpan.FromSeconds(10), (ex, ts) => { Log.Error("Error connecting to DB. Retrying in 10 sec."); })
                    //        .Execute(() => DBInitializer.Initialize(dbContext));

                    //    return dbContext;
                    //});

                    services.AddHostedService<EventHandler>();
                })
                .UseConsoleLifetime();

            return hostBuilder;
        }
    }
}
