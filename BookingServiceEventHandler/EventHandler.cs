﻿using BookingServiceEventHandler.Events;
using BookingServiceEventHandler.Models;
using BookingServiceEventHandler.Repository;
using Infrastructure.Messaging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BookingServiceEventHandler
{
    public class EventHandler : IHostedService, IMessageHandlerCallback
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly IMessageHandler _messageHandler;

        public EventHandler(IBookingRepository bookingRepository, IMessageHandler messageHandler)
        {
            _bookingRepository = bookingRepository;
            _messageHandler = messageHandler;
        }
        public async Task<bool> HandleMessageAsync(string messageType, string message)
        {
            Console.WriteLine("HANDLE MESSAGE ASYNC ", messageType);

            JObject messageObject = MessageSerializer.Deserialize(message);
            try
            {
                switch(messageType)
                {
                    case "CustomerRegistered":
                        await HandleAsync(messageObject.ToObject<CustomerRegistered>());
                        break;
                    case "FlightBooked":
                        await HandleAsync(messageObject.ToObject<FlightBooked>());
                        break;
                    case "FlightRegistered":
                        await HandleAsync(messageObject.ToObject<FlightRegistered>());
                        break;
                }
            }
            catch(Exception ex)
            {
                string messageId = messageObject.Property("MessageId") != null ? messageObject.Property("MessageId").Value<string>() : "[unknown]";
                Log.Error(ex, "Error while handling {MessageType} message with id {MessageId}.", messageType, messageId);
            }
            return true;
        }

        private async Task<bool> HandleAsync(CustomerRegistered e)
        {
            Log.Information("Register Customer: {CustomerId}, {Name}",
                e.CustomerId, e.Name);
            try
            {
                await _bookingRepository.InsertCustomer(new Customer
                {
                    Name = e.Name
                });
            }
            catch (DbUpdateException)
            {
                Log.Warning("Skipped adding customer with customer id {CustomerId}.", e.CustomerId);
            }
            return true;
        }

        private async Task<bool> HandleAsync(FlightBooked e)
        {
            Log.Information("Flight booked: {FlightId}, {Name}",
                e.Flight.FlightId, e.Customer.Name);
            try
            {
                await _bookingRepository.InsertReservation(new Reservation
                {
                    Customer=e.Customer,
                    Flight=e.Flight,
                    Description=e.Description
                });
            }
            catch (DbUpdateException)
            {
                Log.Warning("Skipped adding reservation with flight id {FlightId}.", e.Flight.FlightId);
            }
            return true;
        }

        private async Task<bool> HandleAsync(FlightRegistered e)
        {
            Console.WriteLine("HANDLE ASYNC-upis u bazu FLIGHT", e);

            Log.Information("Register Flight: {FlightId}, {FlightCode}",
                e.FlightId, e.FlightCode);
            try
            {
                await _bookingRepository.InsertFlight(new Flight
                {
                    FlightCode=e.FlightCode,
                    EmptySeets=e.EmptySeets,
                    Price=e.Price,
                    AirlineName = e.AirlineName,
                });
            }
            catch (DbUpdateException)
            {
                Log.Warning("Skipped adding flight with id {FlightId}.", e.FlightId);
            }
            return true;
        }
        public void Start()
        {
            _messageHandler.Start(this);
        }
        public void Stop()
        {
            _messageHandler.Stop();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("START ASYNC ");

            _messageHandler.Start(this);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _messageHandler.Stop();
            return Task.CompletedTask;
        }
    }
}
