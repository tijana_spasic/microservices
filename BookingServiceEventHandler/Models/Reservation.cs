﻿using System;

namespace BookingServiceEventHandler.Models
{
    public class Reservation
    {
        public int ReservationId { get; set; }
        public Customer Customer { get; set; }
        public Flight Flight { get; set; }
        public string Description { get; set; }
        public int NumberOfSeets { get; set; }
    }
}
