﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using CustomerServiceAPI.Commands;
using CustomerServiceAPI.Events;
using CustomerServiceAPI.Mappers;
using CustomerServiceAPI.Models;
using FlightManagmentSystem.Repository;
using Infrastructure.Messaging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FlightManagmentSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMessagePublisher _messagePublisher;

        public CustomerController(ICustomerRepository customerRepository, IMessagePublisher messagePublisher)
        {
            _customerRepository = customerRepository;
            _messagePublisher = messagePublisher;
        }
        //GET: api/Customer
        [HttpGet]
        public async Task<IEnumerable<Customer>> Get()
        {
            return await _customerRepository.GetCustomers();
        }

        [HttpGet]
        [Route("{customerId}", Name = "GetByCustomerId")]
        public async Task<IActionResult> GetByCustomerId(int customerId)
        {
            var customer = await _customerRepository.GetCustomerById(customerId);
            if (customer == null)
            {
                return NotFound();
            }
            return Ok(customer);
        }

        // POST: api/RegisterAsync
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RegisterCustomer command)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // insert customer
                    Customer customer = command.MapToCustomer();
                    await _customerRepository.InsertCustomer(customer);

                    // send event
                    CustomerRegistered e = command.MapToCustomerRegistered();
                    await _messagePublisher.PublishMessageAsync(e.MessageType, e, "");

                    // return result
                    return CreatedAtRoute("GetByCustomerId", new { customerId = customer.CustomerId }, customer);
                }
                return BadRequest();
            }
            catch (DbUpdateException)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
                return StatusCode(StatusCodes.Status500InternalServerError);
                throw;
            }
        }

        // PUT: api/Customer/5
        [HttpPut("{id}")]
        public async void Put(int id, [FromBody] Customer customer)
        {
            if (customer != null)
            {
                using (var scope = new TransactionScope())
                {
                    await _customerRepository.UpdateCustomer(customer);
                    scope.Complete();
                }
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await _customerRepository.DeleteCustomer(id);
        }
    }
}
