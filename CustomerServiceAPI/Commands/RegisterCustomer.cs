﻿using Infrastructure.Messaging;
using System;

namespace CustomerServiceAPI.Commands
{
    public class RegisterCustomer : Command
    {
        public readonly int CustomerId;
        public readonly string Name;
        public readonly string Address;
        public readonly string PostalCode;
        public readonly string City;
        public readonly string TelephoneNumber;
        public readonly string EmailAddress;

        public RegisterCustomer(Guid messageId, int customerId, string name, string address, string postalCode, string city,
            string telephoneNumber, string emailAddress) : base(messageId)
        {
            CustomerId = customerId;
            Name = name;
            Address = address;
            PostalCode = postalCode;
            City = city;
            TelephoneNumber = telephoneNumber;
            EmailAddress = emailAddress;
        }
    }
}
