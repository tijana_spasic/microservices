﻿using Infrastructure.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerServiceAPI.Events
{
    public class CustomerRegistered : Event
    {
        public readonly int CustomerId;
        public readonly string Name;
        public readonly string Address;
        public readonly string PostalCode;
        public readonly string City;
        public readonly string TelephoneNumber;
        public readonly string EmailAddress;

        public CustomerRegistered(Guid messageId, int customerId, string name, string address, string postalCode, string city,
            string telephoneNumber, string emailAddress) : base(messageId, messageType: "CustomerRegistered")
        {
            CustomerId = customerId;
            Name = name;
            Address = address;
            PostalCode = postalCode;
            City = city;
            TelephoneNumber = telephoneNumber;
            EmailAddress = emailAddress;
        }
    }
}
