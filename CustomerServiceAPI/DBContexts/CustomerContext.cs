﻿using CustomerServiceAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace FlightManagmentSystem.DBContexts
{
    public class CustomerContext : DbContext
    {
        public CustomerContext(DbContextOptions<CustomerContext> options) : base(options)
        {
        }
        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasKey(entity => entity.CustomerId);
            modelBuilder.Entity<Customer>().ToTable("Customer");

            base.OnModelCreating(modelBuilder);
        }
    }
}
