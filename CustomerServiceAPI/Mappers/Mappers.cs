﻿using CustomerServiceAPI.Commands;
using CustomerServiceAPI.Events;
using CustomerServiceAPI.Models;
using System;

namespace CustomerServiceAPI.Mappers
{
    public static class Mappers
    {
        public static CustomerRegistered MapToCustomerRegistered(this
            RegisterCustomer command) => new CustomerRegistered
            (
                Guid.NewGuid(),
                command.CustomerId,
                command.Name,
                command.Address,
                command.PostalCode,
                command.City,
                command.TelephoneNumber,
                command.EmailAddress
            );

        public static Customer MapToCustomer(this
            RegisterCustomer command) => new Customer
            {
                Address = command.Address,
                City = command.City,
                EmailAddress = command.EmailAddress,
                Name = command.Name,
                PostalCode = command.PostalCode,
                TelephoneNumber = command.TelephoneNumber,
            };
    }
}
