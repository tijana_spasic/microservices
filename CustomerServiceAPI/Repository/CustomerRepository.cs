﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlightManagmentSystem.DBContexts;
using CustomerServiceAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace FlightManagmentSystem.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly CustomerContext _dbContext;

        public CustomerRepository(CustomerContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<Customer>> GetCustomers()
        {
            return await _dbContext.Customers.ToListAsync();
        }

        public Task DeleteCustomer(int CustomerId)
        {
            var customer = _dbContext.Customers.Find(CustomerId);
            _dbContext.Customers.Remove(customer);
            return SaveAsync();
        }

        public Task<Customer> GetCustomerById(int CustomerId)
        {
             return _dbContext.Customers.FindAsync(CustomerId).AsTask();
        }

        public async Task InsertCustomer(Customer Customer)
        {
            await _dbContext.Customers.AddAsync(Customer);
            await SaveAsync();
        }

        public Task SaveAsync()
        {
            return _dbContext.SaveChangesAsync();
        }

        public Task UpdateCustomer(Customer Customer)
        {
            _dbContext.Entry(Customer).State = EntityState.Modified;
            return SaveAsync();
        }
    }
}
