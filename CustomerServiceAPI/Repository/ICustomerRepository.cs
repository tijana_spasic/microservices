﻿using CustomerServiceAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightManagmentSystem.Repository
{
    public interface ICustomerRepository
    {
        Task<IEnumerable<Customer>> GetCustomers();
        Task<Customer> GetCustomerById(int CustomerId);
        Task InsertCustomer(Customer Customer);
        Task DeleteCustomer(int CustomerId);
        Task UpdateCustomer(Customer Customer);
        Task SaveAsync();
    }
}
