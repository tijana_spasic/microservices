﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Messaging
{
    public class Message
    {
        public readonly Guid MessageId;
        public readonly string MessageType;

        public Message() : this(Guid.NewGuid())
        {
        }
        public Message(Guid mesaageId)
        {
            MessageId = mesaageId;
        }
        public Message(string messageType) :this(Guid.NewGuid())
        {
            MessageType = messageType;
        }
        public Message(Guid messageId, string messageType)
        {
            MessageId = messageId;
            MessageType = messageType;
        }
    }
}
