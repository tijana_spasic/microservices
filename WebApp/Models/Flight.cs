﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace WebApp.Models
{
    public class Flight
    {
        
        public int FlightId { get; set; }
        [Required]
        [Display(Name = "Flight code")]
        public string FlightCode { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string AirlineName { get; set; }
        public int Duration { get; set; }
        public int Price { get; set; }
        [Required]
        [Display(Name = "Empty seets")]
        public int EmptySeets { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        public DateTime FlightTime { get; set; }

        public Flight()
        {
            FlightTime = DateTime.UtcNow;
        }
    }
}
