﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class BookingCustomer
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
    }
}
