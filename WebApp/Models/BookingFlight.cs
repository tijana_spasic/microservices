﻿namespace WebApp.Models
{
    public class BookingFlight
    {
        public int FlightId { get; set; }
        public string FlightCode { get; set; }
        public int EmptySeets { get; set; }
        public int Price { get; set; }
        public string AirlineName { get; set; }
    }
}
