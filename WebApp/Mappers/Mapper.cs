﻿using System;
using WebApp.Commands;
using WebApp.ViewModels;

namespace WebApp.Mappers
{
    public static class Mapper
    {
        public static RegisterCustomer MapToRegisterCustomer(this CustomerNewViewModel source) => new RegisterCustomer
        (
            Guid.NewGuid(),
            2,
            source.Customer.Name,
            source.Customer.Address,
            source.Customer.PostalCode,
            source.Customer.City,
            source.Customer.TelephoneNumber,
            source.Customer.EmailAddress
        );

        public static RegisterFlight MapToRegisterFlight(this FlightNewViewModel source) => new RegisterFlight
            (
            Guid.NewGuid(),
            2,
            source.Flight.FlightCode,
            source.Flight.From,
            source.Flight.To,
            source.Flight.AirlineName,
            source.Flight.Duration,
            source.Flight.Price,
            source.Flight.EmptySeets,
            source.Flight.FlightTime
            );

        public static BookedFlight MapToBookedFlight(this FlightBookingViewModel source) => new BookedFlight
        (
            Guid.NewGuid(),
            1,
            source.Flight,
            source.Customer,
            source.Description,
            source.SelectedNumberOfSeets
        );
    }
}
