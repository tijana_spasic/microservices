﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Refit;
using WebApp.Commands;
using WebApp.Models;

namespace WebApp.RESTClients
{
    public class CustomerServiceAPI : ICustomerServiceAPI
    {
        private ICustomerServiceAPI _restClient;

        public CustomerServiceAPI(IConfiguration config, HttpClient httpClient)
        {
            string apiHostAndPort = config.GetSection("APIServiceLocations").GetValue<string>(
                "CustomerServiceAPI");
            httpClient.BaseAddress = new Uri($"http://{apiHostAndPort}/api");
            _restClient = RestService.For<ICustomerServiceAPI>(httpClient);
        }
        public async Task<Customer> GetCustomerById([AliasAs("id")] int customerId)
        {
            try
            {
                return await _restClient.GetCustomerById(customerId);
            }
            catch (ApiException ex)
            {
                if (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task<List<Customer>> GetCustomers()
        {
            return await _restClient.GetCustomers();
        }

        public async Task RegisterCustomer(RegisterCustomer command)
        {
            await _restClient.RegisterCustomer(command);
        }
    }
}
