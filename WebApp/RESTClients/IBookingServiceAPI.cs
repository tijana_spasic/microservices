﻿using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Commands;
using WebApp.Models;

namespace WebApp.RESTClients
{
    public interface IBookingServiceAPI
    {
        [Get("/booking/GetByFlightId/{id}")]
        Task<BookingFlight> GetFlightById([AliasAs("id")] int flightId);

        [Get("/booking/GetByFlightCode")]
        Task<BookingFlight> GetFlightByCode(string flightCode);

        [Get("/booking/GetByCustomerId/{id}")]
        Task<BookingCustomer> GetCustomerById([AliasAs("id")] int customerId);

        [Get("/booking")]
        Task<List<BookingCustomer>> GetCustomers();

        [Post("/booking")]
        Task BookAFlight(BookedFlight command);
    }
}
