﻿using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Commands;
using WebApp.Models;

namespace WebApp.RESTClients
{
    public interface ICustomerServiceAPI
    {
        [Get("/customer")]
        Task<List<Customer>> GetCustomers();

        [Get("/customer/{id}")]
        Task<Customer> GetCustomerById([AliasAs("id")] int customerId);

        [Post("/customer")]
        Task RegisterCustomer(RegisterCustomer command);
    }
}
