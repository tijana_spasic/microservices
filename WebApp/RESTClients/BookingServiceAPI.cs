﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Refit;
using WebApp.Commands;
using WebApp.Models;

namespace WebApp.RESTClients
{
    public class BookingServiceAPI : IBookingServiceAPI
    {
        private IBookingServiceAPI _restClient;
        public BookingServiceAPI(IConfiguration config, HttpClient httpClient)
        {
            string apiHostAndPort = config.GetSection("APIServiceLocations").GetValue<string>(
                "BookingServiceAPI");
            httpClient.BaseAddress = new Uri($"http://{apiHostAndPort}/api");
            _restClient = RestService.For<IBookingServiceAPI>(httpClient);
        }
        public async Task<BookingFlight> GetFlightById([AliasAs("id")] int flightId)
        {
            try
            {
                return await _restClient.GetFlightById(flightId);
            }
            catch (ApiException ex)
            {
                if (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task<List<BookingCustomer>> GetCustomers()
        {
            return await _restClient.GetCustomers();
        }

        public async Task BookAFlight(BookedFlight command)
        {
            await _restClient.BookAFlight(command);
        }

        public async Task<BookingCustomer> GetCustomerById([AliasAs("id")] int customerId)
        {
            try
            {
                return await _restClient.GetCustomerById(customerId);
            }
            catch (ApiException ex)
            {
                if (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task<BookingFlight> GetFlightByCode(string flightCode)
        {
            try
            {
                return await _restClient.GetFlightByCode(flightCode);
            }
            catch (ApiException ex)
            {
                if (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }
    }
}
