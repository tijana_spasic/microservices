﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Refit;
using WebApp.Commands;
using WebApp.Models;

namespace WebApp.RESTClients
{
    public class FlightServiceAPI : IFlightServiceAPI
    {
        private IFlightServiceAPI _restClient;

        public FlightServiceAPI(IConfiguration config, HttpClient httpClient)
        {
            string apiHostAndPort = config.GetSection("APIServiceLocations").GetValue<string>(
                "FlightServiceAPI");
            httpClient.BaseAddress = new Uri($"http://{apiHostAndPort}/api");
            _restClient = RestService.For<IFlightServiceAPI>(httpClient);
        }
        public async Task<Flight> GetFlightById([AliasAs("id")] int flightId)
        {
            try
            {
                return await _restClient.GetFlightById(flightId);
            }
            catch (ApiException ex)
            {
                if (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task<List<Flight>> GetFlights()
        {
            return await _restClient.GetFlights();
        }

       
        public async Task RegisterFlight(RegisterFlight command)
        {
            await _restClient.RegisterFlight(command);
        }
        public async Task<List<Flight>> SearchFlight(string from, string to, DateTime date)
        {
            return await _restClient.SearchFlight(from, to, date);
        }
    }
}
