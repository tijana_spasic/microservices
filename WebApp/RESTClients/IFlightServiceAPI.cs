﻿using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Commands;
using WebApp.Models;

namespace WebApp.RESTClients
{
    public interface IFlightServiceAPI
    {
        [Get("/flight")]
        Task<List<Flight>> GetFlights();

        [Get("/flight/{id}")]
        Task<Flight> GetFlightById([AliasAs("id")] int flightId);

        [Get("/flight/SearchFlight")]
        Task<List<Flight>> SearchFlight(string from, string to, DateTime date);

        [Post("/flight")]
        Task RegisterFlight(RegisterFlight command);
    }
}
