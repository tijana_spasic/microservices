﻿using WebApp.Models;

namespace WebApp.ViewModels
{
    public class FlightNewViewModel
    {
        public Flight Flight { get; set; }
    }
}
