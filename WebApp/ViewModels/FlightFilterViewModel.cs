﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels
{
    public class FlightFilterViewModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public DateTime Date { get; set; }
    }
}
