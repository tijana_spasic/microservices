﻿using WebApp.Models;

namespace WebApp.ViewModels
{
    public class CustomerNewViewModel
    {
        public Customer Customer { get; set; }
    }
}
