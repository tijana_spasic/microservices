﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApp.Models;

namespace WebApp.ViewModels
{
    public class FlightBookingViewModel
    {
        public BookingFlight Flight { get; set; } 
        public int FlightId { get; set; } 
        public IEnumerable<SelectListItem> Customers { get; set; }
        public BookingCustomer Customer { get; set; } 
        public string CustomerName { get; set; }

        [Required(ErrorMessage = "Customer is required")]
        public int SelectedCustomerId { get; set; }
        public IEnumerable<SelectListItem> Numbers { get; set; }
        public int SelectedNumberOfSeets { get; set; }
        public string Description { get; set; }
    }
}
