﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using WebApp.Commands;
using WebApp.Mappers;
using WebApp.RESTClients;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    public class BookingServiceController : Controller
    {
        private readonly IBookingServiceAPI _bookingServiceAPI;
        private readonly ILogger _logger;
        private ResiliencyHelper _resiliencyHelper;

        public BookingServiceController(IBookingServiceAPI bookingServiceAPI,
            ILogger<BookingServiceController> logger)
        {
            _bookingServiceAPI = bookingServiceAPI;
            _logger = logger;
            _resiliencyHelper = new ResiliencyHelper(_logger);
        }

        [HttpGet]
        public async Task<IActionResult> BookAFlight(string id)
        {
            return await _resiliencyHelper.ExecuteResilient(async () =>
            {
                var model = new FlightBookingViewModel
                {
                    Flight = await _bookingServiceAPI.GetFlightByCode(id),
                    Customers = await GetAvailableCustomerList(),
                    Numbers = GetRange()
                };
                return View(model);
            }, View("Offline", new FlightBookingOfflineViewModel()));
        }

        [HttpPost]
        public async Task<IActionResult> BookAFlight([FromForm] FlightBookingViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Flight =await _bookingServiceAPI.GetFlightById(model.FlightId);
                model.Customer = await _bookingServiceAPI.GetCustomerById(model.SelectedCustomerId);

                return await _resiliencyHelper.ExecuteResilient(async () =>
                {
                    BookedFlight cmd = model.MapToBookedFlight();
                    await _bookingServiceAPI.BookAFlight(cmd);
                   return RedirectToAction("Index", "FlightService");
                }, View("Offline", new FlightBookingOfflineViewModel()));
            }
            else
            {
                return View("BookAFlight", model);
            }
        }

        private async Task<IEnumerable<SelectListItem>> GetAvailableCustomerList()
        {
            var customers = await _bookingServiceAPI.GetCustomers();
            return customers.Select(c =>
                new SelectListItem
                {
                    Value = c.CustomerId.ToString(),
                    Text = $"{c.Name}"
                });
        }

        private IEnumerable<SelectListItem> GetRange()
        {
            return Enumerable.Range(1, 5)
                .Select(n => new SelectListItem
                {
                    Value = n.ToString(),
                    Text = n.ToString()
                });
        }
    }
}