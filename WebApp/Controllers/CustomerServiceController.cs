﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Commands;
using WebApp.Mappers;
using WebApp.Models;
using WebApp.RESTClients;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    public class CustomerServiceController : Controller
    {
        private readonly ICustomerServiceAPI _customerServiceAPI;
        private readonly ILogger _logger;
        private ResiliencyHelper _resiliencyHelper;

        public CustomerServiceController(ICustomerServiceAPI customerServiceAPI,
            ILogger<CustomerServiceController> logger)
        {
            _customerServiceAPI = customerServiceAPI;
            _logger = logger;
            _resiliencyHelper = new ResiliencyHelper(_logger);
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return await _resiliencyHelper.ExecuteResilient(async () =>
            {
                var model = new CustomerViewModel
                {
                    Customers = await _customerServiceAPI.GetCustomers()
                };
                return View(model);
            }, View("Offline", new CustomerOfflineViewModel()));
        }

        [HttpGet]
        public IActionResult New()
        {
            var model = new CustomerNewViewModel
            {
                Customer = new Customer()
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromForm] CustomerNewViewModel inputModel)
        {
            if (ModelState.IsValid)
            {
                return await _resiliencyHelper.ExecuteResilient(async () =>
                {
                    RegisterCustomer cmd = inputModel.MapToRegisterCustomer();
                    await _customerServiceAPI.RegisterCustomer(cmd);
                    return RedirectToAction("Index");
                }, View("Offline", new CustomerOfflineViewModel()));
            }
            else
            {
                return View("New", inputModel);
            }
        }
    }
}