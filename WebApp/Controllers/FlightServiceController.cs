﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Commands;
using WebApp.Mappers;
using WebApp.Models;
using WebApp.RESTClients;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    public class FlightServiceController : Controller
    {
        private readonly IFlightServiceAPI _flightServiceAPI;
        private readonly ILogger _logger;
        private ResiliencyHelper _resiliencyHelper;

        public FlightServiceController(IFlightServiceAPI flightServiceAPI,
            ILogger<FlightServiceController> logger)
        {
            _flightServiceAPI = flightServiceAPI;
            _logger = logger;
            _resiliencyHelper = new ResiliencyHelper(_logger);
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return await _resiliencyHelper.ExecuteResilient(async () =>
            {
                var model = new FlightViewModel
                {
                    Flights = await _flightServiceAPI.GetFlights()
                };
                return View(model);
            }, View("Offline", new FlightOfflineViewModel()));
        }

        [HttpPost]
        public async Task<IActionResult> SearchFlight(FlightFilterViewModel filterModel)
        {
            return await _resiliencyHelper.ExecuteResilient(async () =>
            {
                var model = new FlightViewModel
                {
                    Flights = await _flightServiceAPI.SearchFlight(filterModel.From,filterModel.To,filterModel.Date)
                };
                return View(nameof(Index), model);
            }, View("Offline", new FlightOfflineViewModel()));
        }

        [HttpGet]
        public IActionResult New()
        {
            var model = new FlightNewViewModel
            {
                Flight = new Flight()
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromForm] FlightNewViewModel inputModel)
        {
            if (ModelState.IsValid)
            {
                return await _resiliencyHelper.ExecuteResilient(async () =>
                {
                    RegisterFlight cmd = inputModel.MapToRegisterFlight();
                    await _flightServiceAPI.RegisterFlight(cmd);
                    return RedirectToAction("Index");
                }, View("Offline", new FlightOfflineViewModel()));
            }
            else
            {
                return View("New", inputModel);
            }
        }
    }
}