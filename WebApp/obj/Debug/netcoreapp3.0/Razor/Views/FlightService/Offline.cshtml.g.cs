#pragma checksum "C:\Users\Tijana\source\repos\FlightManagmentSystem\WebApp\Views\FlightService\Offline.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "84773c7feb251a6e5ece5ad88a4a61be5349fd85"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_FlightService_Offline), @"mvc.1.0.view", @"/Views/FlightService/Offline.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Tijana\source\repos\FlightManagmentSystem\WebApp\Views\_ViewImports.cshtml"
using WebApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Tijana\source\repos\FlightManagmentSystem\WebApp\Views\_ViewImports.cshtml"
using WebApp.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"84773c7feb251a6e5ece5ad88a4a61be5349fd85", @"/Views/FlightService/Offline.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fc48f17eb9bac3476d8060730298bf398eb2fa5e", @"/Views/_ViewImports.cshtml")]
    public class Views_FlightService_Offline : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<WebApp.ViewModels.FlightOfflineViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\Tijana\source\repos\FlightManagmentSystem\WebApp\Views\FlightService\Offline.cshtml"
  
    ViewData["Title"] = "Offline";

#line default
#line hidden
#nullable disable
            WriteLiteral("<h3 id=\"PageTitle\">Flight Management - offline</h3>\r\n<p>Unfortunately the flight management module is offline at the moment. Try again soon.</p>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<WebApp.ViewModels.FlightOfflineViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
