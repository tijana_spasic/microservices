﻿using Infrastructure.Messaging;
using System;
using WebApp.Models;

namespace WebApp.Commands
{
    public class BookedFlight : Command
    {
        public readonly int BookedFlightId;
        public readonly BookingFlight Flight;
        public readonly BookingCustomer Customer;
        public readonly string Description;
        public readonly int NumberOfSeets;
        public BookedFlight(Guid messageId, int id, BookingFlight flight, BookingCustomer customer,string description, int numOfSeets) :base(messageId)
        {
            BookedFlightId = id;
            Flight = flight;
            Customer = customer;
            Description = description;
            NumberOfSeets = numOfSeets;
        }
    }
}
