﻿using BookingServiceAPI.Models;
using Infrastructure.Messaging;
using System;

namespace BookingServiceAPI.Commands
{
    public class BookedFlight : Command
    {
        public readonly int BookedFlightId;
        public readonly Flight Flight;
        public readonly Customer Customer;
        public readonly string Description;
        public readonly int NumberOfSeets;
        public BookedFlight(Guid messageId, int id, Flight flight, Customer customer, string description, int numberOdSeets) : base(messageId)
        {
            BookedFlightId = id;
            Flight = flight;
            Customer = customer;
            Description = description;
            NumberOfSeets = numberOdSeets;
        }
    }
}
