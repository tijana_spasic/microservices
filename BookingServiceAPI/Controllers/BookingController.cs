﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookingServiceAPI.Commands;
using BookingServiceAPI.Events;
using BookingServiceAPI.Mappers;
using BookingServiceAPI.Models;
using BookingServiceAPI.Repositories;
using Infrastructure.Messaging;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace BookingServiceAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        private readonly IFlightRepository _flightRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IMessagePublisher _messagePublisher;


        public BookingController(IFlightRepository flightRepository, ICustomerRepository customerRepository, IMessagePublisher messagePublisher)
        {
            _flightRepository = flightRepository;
            _customerRepository = customerRepository;
            _messagePublisher = messagePublisher;
        }

        [HttpGet]
        [Route("GetByFlightId/{flightId}")]
        public async Task<IActionResult> GetByFlightId(int flightId)
        {
            var flight = await _flightRepository.GetFlightById(flightId);
            if (flight == null)
            {
                return NotFound();
            }
            return Ok(flight);
        }

        //GetByFlightCode
        [HttpGet]
        [Route("GetByFlightCode")]
        public async Task<IActionResult> GetByFlightCode(string flightCode)
        {
            var flight = await _flightRepository.GetFlightByCode(flightCode);
            if (flight == null)
            {
                return NotFound();
            }
            return Ok(flight);
        }

        [HttpGet]
        [Route("GetByCustomerId/{customerId}")]
        public async Task<IActionResult> GetByCustomerId(int customerId)
        {
            var customer = await _customerRepository.GetCustomerById(customerId);
            if (customer == null)
            {
                return NotFound();
            }
            return Ok(customer);
        }

        [HttpGet]
        public async Task<IEnumerable<Customer>> Get()
        {
            return await _customerRepository.GetCustomersAsync();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] BookedFlight command)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    FlightBooked e = command.MapToFlightBooked();
                    await _messagePublisher.PublishMessageAsync(e.MessageType,e,"");

                    return Ok(command);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                    throw;
                }
            }
            return BadRequest();
        }
    }
}