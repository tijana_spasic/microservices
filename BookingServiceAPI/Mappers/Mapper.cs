﻿using BookingServiceAPI.Commands;
using BookingServiceAPI.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingServiceAPI.Mappers
{
    public static class Mapper
    {
        public static FlightBooked MapToFlightBooked(this BookedFlight command) => new FlightBooked
           (
            Guid.NewGuid(),
            command.BookedFlightId,
            command.Flight,
            command.Customer,
            command.Description,
            command.NumberOfSeets
            );
    }
}
