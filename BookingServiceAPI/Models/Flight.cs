﻿using System;

namespace BookingServiceAPI.Models
{
    public class Flight
    {
        public int FlightId { get; set; }
        public string FlightCode { get; set; }
        public int EmptySeets { get; set; }
        public int Price { get; set; }
        public string AirlineName { get; set; }
    }
}
