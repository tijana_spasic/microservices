﻿using BookingServiceAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace BookingServiceAPI.DBContexts
{
    public class FlightContext : DbContext
    {
        public FlightContext(DbContextOptions<FlightContext> options) : base(options)
        {
        }
        public DbSet<Flight> Flights { get; set; }
    }
}
