﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingServiceAPI.DBContexts;
using BookingServiceAPI.Models;

namespace BookingServiceAPI.Repositories
{
    public class FlightRepository : IFlightRepository
    {
        private readonly FlightContext _dbContext;
        public FlightRepository(FlightContext flightContext)
        {
            _dbContext = flightContext;
        }

        public Task<Flight> GetFlightByCode(string flightCode)
        {
            throw new NotImplementedException();
        }

        public Task<Flight> GetFlightById(int FlightId)
        {
            return _dbContext.Flights.FindAsync(FlightId).AsTask();
        }
    }
}
