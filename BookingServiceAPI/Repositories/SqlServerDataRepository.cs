﻿using BookingServiceAPI.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;

namespace BookingServiceAPI.Repositories
{
    public class SqlServerDataRepository : IFlightRepository, ICustomerRepository
    {
        private string _connectionString;
        public SqlServerDataRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IEnumerable<Customer>> GetCustomersAsync()
        {
            List<Customer> customers = new List<Customer>();
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                try
                {
                    var customersSelection = await conn.QueryAsync<Customer>("select * from Customer");

                    if (customersSelection != null)
                    {
                        customers.AddRange(customersSelection);
                    }
                }
                catch (SqlException ex)
                {
                    HandleSqlException(ex);
                }
            }

            return customers;
        }
        public async Task<Flight> GetFlightById(int FlightId)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                try
                {
                    return await conn.QueryFirstOrDefaultAsync<Flight>("select * from Flight where FlightId=@FlightId", new { FlightId = FlightId });
                }
                catch (SqlException ex)
                {
                    HandleSqlException(ex);
                }

                return null;
            }
        }

        public async Task<Flight> GetFlightByCode(string flightCode)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                try
                {
                    return await conn.QueryFirstOrDefaultAsync<Flight>("select * from Flight where FlightCode=@FlightCode", new { FlightCode = flightCode });
                }
                catch (SqlException ex)
                {
                    HandleSqlException(ex);
                }

                return null;
            }
        }

        public async Task<Customer> GetCustomerById(int customerId)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                try
                {
                    return await conn.QueryFirstOrDefaultAsync<Customer>("select * from Customer where CustomerId=@CustomerId", new { CustomerId = customerId });
                }
                catch (SqlException ex)
                {
                    HandleSqlException(ex);
                }

                return null;
            }
        }

        private void HandleSqlException(SqlException ex)
        {
            throw new NotImplementedException();
        }
    }
}
