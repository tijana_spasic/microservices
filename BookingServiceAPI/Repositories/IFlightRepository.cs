﻿using BookingServiceAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingServiceAPI.Repositories
{
    public interface IFlightRepository
    {
        Task<Flight> GetFlightById(int FlightId);
        Task<Flight> GetFlightByCode(string flightCode);

    }
}
